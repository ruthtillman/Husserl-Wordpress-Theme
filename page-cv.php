<?php get_header(); ?>
<?php get_template_part( 'navigation' ); ?>
<div class="container">
	<div class="col-md-9 col-sm-12 centerdiv entry">
<p class="alignright">[<a href="http://micahtillman.com/wp-content/uploads/2014/11/Tillman-CV-Web.pdf">Click here for PDF format</a>]</p>
<p>&nbsp;</p>
<h2 class="cv">Micah D. Tillman &mdash; Curriculum Vitae</h2>
<hr />
<p class="aligncenter">micahtillman@<span class="displaynone">[removethis]</span>gmail.com</p>
<p><strong>Academic Positions</strong></p>
<ul>
<li>Adjunct Lecturer
<ul>
<li>McDaniel College . . . . . . . . . . . . . . . . . . . Spring 2013–Present</li>
<li>University of Maryland, College Park . . . Fall 2011 and Present</li>
<li>Mount St. Mary&#8217;s University . . . . . . . . . . . Fall 2012–Spring 2014</li>
<li>The Catholic University of America . . . . . Fall 2011–Spring 2012</li>
</ul>
</li>
<li>Graduate Teaching Fellow
<ul>
<li>The Catholic University of America . . . . . Fall 2006–Spring 2011</li>
</ul>
</li>
</ul>
<p><strong>Education</strong></p>
<ul>
<li>Ph.D., Philosophy: The Catholic University of America . . . . . . . . . . . . . . . . . May, 2011</li>
<p class="indent60">Dissertation: &#8220;Empty and Filled Intentions in Husserl&#8217;s Early Work&#8221;<br />
Director: Dr. Robert Sokolowski</p>

<li>M.A., Philosophy: West Chester University of Pennsylvania . . . . . . . . . . . . . May, 2004</li>

<p class="indent60">Thesis: &#8220;Whence Value?: Seeking Value&#8217;s Source&#8221;<br />
Director: Dr. Seetha Burtner</p>

<li>B.A., Computer Science: Messiah College . . . . . . . . . . . . . . . . . . . . . . . . . . . May, 2002</li>

<p class="indent60">Minor: Music</p>
</ul>
<p><strong>Area of Specialization</strong><strong> </strong></p>
<ul>
<li>Phenomenology (Husserl, Heidegger)</li>
</ul>
<p><strong>Areas of Competence</strong><strong> </strong></p>
<ul>
<li>History of Philosophy (including Modern)</li>
<li>Epistemology (Classical–Contemporary)</li>
<li>Ethics (Theoretical and Applied)</li>
<li>Logic</li>
</ul>
<hr class="cv"/>
<h2 class="cv">Scholarship</h2>
<p><strong>Research Projects</strong><strong> </strong></p>
<ul>
<li>Meaning: I am developing a comprehensive phenomenology of semiotic, mental, and existential meaning (including &#8220;meaningless&#8221; mathematical signs and mystery—a structural variant of meaning—in art, nature, and theology).</li>
</ul>
<ul>
<li>Action: I am working to expand and defend an account of human action and freedom that focuses on changes and origins, rather than causation and possibility.</li>
</ul>
<ul>
<li>Priority: My initial work toward a systematic theory of priority—elucidating its types, the relations between those types, and the value of each type in philosophical argumentation—is currently under review at <em>Metaphilosophy</em>.</li>
</ul>
<p><strong>Academic Publications </strong></p>
<ul>
<li>&#8220;Dallas Willard&#8217;s Husserlian Phenomenology,&#8221; in Carlo Ierna and Michaela Ferreri, eds., <em>The Reception of Edmund Husserl&#8217;s Thought in North America</em>, Contributions to Phenomenology (Dordrecht: Springer, forthcoming).</li>
</ul>
<ul>
<li>&#8220;Acts as Changes: A ‘Metabolic&#8217; Approach to the Philosophy of Action,&#8221; in Michael Sigrist and Roman Altshuler, eds., <em>Time and the Philosophy of Action</em> (New York: Routledge, forthcoming).</li>
</ul>
<ul>
<li>&#8220;Genesis 1&#8242;s Solution to the Euthyphro Problem,&#8221; <em>Philosophy &amp; Theology</em> 26, no. 1 (2014): 207–19.</li>
</ul>
<ul>
<li>&#8220;Husserl&#8217;s Mereological Semiotics: Indication, Expression, Surrogation,&#8221; <em>The New Yearbook for Phenomenology and Phenomenological Philosophy</em> 12 (2012): 69–108.</li>
</ul>
<ul>
<li>&#8220;<a href="http://www.americandialectic.org/volume-ii-2012/no-2-may/husserls-genetic-philosophy-of-arithmetic/">Husserl&#8217;s Genetic Philosophy of Arithmetic</a>,&#8221; <em>American Dialectic</em> 2, no. 2 (May 2012): 141–90.</li>
</ul>
<p><strong>Papers under Review</strong></p>
<ul>
<li>&#8220;The Practice, Possibility, and Prospects of Philosophy in Light of Contemporary Culture and Science: An Explanation and Defence,&#8221; submitted to <em>Philosophy</em> (in response to the Royal Institute of Philosophy&#8217;s 2014 Essay Prize question: &#8220;What is philosophy? How is it possible? What can it expect to achieve?&#8221;) on August 4<sup>th</sup>, 2014.</li>
</ul>
<ul>
<li>&#8220;How Philosophers Appeal to Priority to Effect Revolution,&#8221; submitted to <em>Metaphilosophy</em> on July 2<sup>nd</sup>, 2014.</li>
</ul>
<p><strong>Academic Presentations</strong></p>
<ul>
<li>&#8220;How to Reconcile Our Contradictory Experiences of Meaning: The Mereology of Significance and Importance&#8221; [abstract accepted, but unable to attend conference]</li>
<p class="indent60">Conference: &#8220;Phenomenology and the Problem of Meaning in Human Life and History,&#8221; Organization of Phenomenological Organizations V<br />
Perth, Australia: Murdoch University, December 8<sup>th</sup>–12<sup>th</sup>, 2014</p>

<li>&#8220;The Embarrassment of Punching Puppets: An Argument from Conversation for Freedom&#8221;</li>

<p class="indent60">Conference: &#8220;Free Will&#8221;<br />
Flint, MI: Center for Cognition and Neuroethics, October 10<sup>th</sup>–11<sup>th</sup>, 2014</p>

<li>&#8220;Acts as Changes: A ‘Metabolic&#8217; Approach to the Philosophy of Action&#8221;</li>

<p class="indent60">Conference: &#8220;Time and Agency&#8221;<br />
Washington, DC: George Washington University, November 18<sup>th</sup>, 2011</p>

<li>&#8220;Do Numerals Mean Anything?: Husserl&#8217;s Theory of Signs in ‘Mechanical&#8217; Calculation&#8221;</li>

<p class="indent60">Conference: &#8220;The Early Phenomenology of Munich and Göttingen&#8221;<br />
Steubenville, OH: Franciscan University of Steubenville, April 30<sup>th</sup>, 2011</p>

<li>&#8220;Do Numerals Mean Anything?: The Practice of ‘Mechanical&#8217; Calculation in Husserl&#8217;s Philosophy of Arithmetic&#8221;</li>

<p class="indent60">Conference: 1<sup>st</sup> Intl. Meeting of the Association for the Philosophy of Mathematical Practice<br />
Brussels, Belgium: University Foundation, VU Brussel, December 11<sup>th</sup>, 2010</p>

<li>&#8220;Locke&#8217;s Ontology of Power and the Roots of the Progressive/Conservative Rift&#8221;</li>

<p class="indent60">Conference: 11<sup>th</sup> Boston College Graduate Philosophy Conference: &#8220;Power&#8221;<br />
Boston, MA: Boston College, March 20<sup>th</sup>, 2010</p>

<li>&#8220;Flight from an Ontology of Hope to a Phenomenology of Political Unity: Husserl as Good Samaritan&#8221;</li>

<p class="indent60">Conference: First Annual Duquesne University Graduate Philosophy Conference: &#8220;Symbioses: Political Ontology and a New Metaphysics&#8221;<br />
Pittsburgh, PA: Duquesne University, March 24<sup>th</sup>, 2007</p>

<li>&#8220;Finding James Ready-to-Hand: Introducing Heidegger and Levinas through James&#8221;</li>

<p class="indent60">Conference: Annual Conference, Pennsylvania State System of Higher Education Interdisciplinary Association for Philosophy and Religious Studies<br />
Panel: &#8220;Perspectives in Contemporary Continental Philosophy: Phenomenology, Pragmatism, Personalism&#8221;<br />
Cheyney, PA: Cheyney University of Pennsylvania, April 3<sup>rd</sup>, 2004</p>
</ul>

<p><strong>Commentary/Responses</strong></p>
<ul>
<li>Response to Evan Ponton&#8217;s &#8220;Edith Stein on the Constitution of the Person in Emotional Experiences&#8221;<strong></strong></li>

<p class="indent60">Conference: CUA Graduate Philosophy Conference: &#8220;Emotion&#8221;<br />
Washington, DC: The Catholic University of America, March 30<sup>th</sup>, 2012</p>
</ul>
<p><strong>Sessions Chaired/Moderated</strong></p>
<ul>
<li>Session 7b<strong></strong></li>
<p class="indent60">Conference: &#8220;Free Will&#8221;<br />
Flint, MI: Center for Cognition and Neuroethics, October 11<sup>th</sup>, 2014</p>
</ul>
<p><strong>Languages </strong></p>
<ul>
<li>French (reading knowledge)</li>
<li>German (reading knowledge)</li>
</ul>
<hr class="cv"/>
<h2 class="cv">Teaching</h2>
<p><strong>Educational Projects</strong></p>
<ul>
<li>Software for College Students
<ul>
<li><em>Chambergon Battle Logic</em>. A method for teaching symbolic logic proofs and truth tables in the form of pictographic games, as well as a computer program students can use to play the games and an e-text that explains the system.</li>
</ul>
</li>
<li>Textbooks for Homeschoolers
<ul>
<li><em>Philosophy in Four Questions: A Thematic Introduction to Philosophy for High School and Beyond</em> (Newark, DE: 7 Sisters IHH, LLC, forthcoming).</li>
<li><em>Song Lyrics: Principles of the Lyricist&#8217;s Art</em> (Newark, DE: 7 Sisters IHH, LLC, forthcoming).</li>
</ul>
</li>
</ul>
<p><strong>Courses Developed</strong></p>
<ul>
<li>&#8220;Philosophy of Music,&#8221; Spring 2014, McDaniel College.</li>
</ul>
<p><strong>Courses Taught</strong></p>
<ul>
<li>McDaniel College: Adjunct Lecturer</li>
<ul>
<li>Aesthetics: PHI 2265, &#8220;Philosophy of Music&#8221; . . . . . . . . . . . . . . . . . . . . . . . Spring 2014</li>
<p class="indent90">Students&#8217; evaluation: No numerical data.</p>
<li>Critical Thinking: PHI 1102, &#8220;Critical Thinking&#8221; . . . . . . . . . . . . . . . . . . . . . Spring 2015</li>
<p class="indent90">Students&#8217; evaluation: No numerical data.</p>
<li>Intro: PHI 1101, &#8220;Introduction to Philosophy&#8221; . . . . . . . . . . . . . . . . . . . . . . Spring 2013</li>
<p class="indent90">Students&#8217; evaluation: No numerical data.</p>
<li>Logic: PHI 2233, &#8220;Elementary Logic&#8221; . . . . . . . . . . . . . . . . . . . . . . . .Springs 2013–2015</li>
<p class="indent90">Students&#8217; evaluation: No numerical data.</p>
<li>Philosophy of Language: PHI 3321, &#8220;Minds and Language&#8221; . . . . . . . . . . . . . . Fall 2014</li>
<p class="indent90">Students&#8217; evaluation: No numerical data.</p>
<li>Philosophy of Mind: PHI 2221, &#8220;Minds and Machines&#8221; . . . . . . . . . . . . . . . . . . Fall 2013</li>
<p class="indent90">Students&#8217; evaluation: No numerical data.</p>
</ul>
<li>University of Maryland College Park: Adjunct Lecturer</li>
<ul>
<li>Ethics: PHIL 140, &#8220;Contemporary Moral Issues&#8221; . . . . . . . . . . . . . . . . . . . . . . . Fall 2014</li>
<p class="indent90">Students&#8217; evaluation: Semester not yet complete.</p>
<li>Existentialism: PHIL 324, &#8220;Existentialism&#8221; . . . . . . . . . . . . . . . . . . . . . . . . . . . Fall 2011</li>
<p class="indent90">Students&#8217; evaluation: 3.8 out of 4</p>
</ul>
<li>Mount St. Mary&#8217;s University: Adjunct Lecturer</li>
<ul>
<li>Classical: PHIL 211, &#8220;From Cosmos to Citizen&#8221; . . . . . . . . . . . . . . . . . . . . . . . . Fall 2012</li>
<p class="indent90">Students&#8217; evaluation: 4.75 out of 5</p>
<p class="indent90">VTPH 103, &#8220;Classical Philosophy&#8221; . . . . . . . . . . . . . . . . . . . Spring 2013, 2014</p>
<p class="indent90">Students&#8217; evaluation: 4.98 out of 5</p>
<li>Modern: PHIL 212, &#8220;From Self to Society&#8221; . . . . . . . . . . . . . . . . . . . . . . . . Summer 2013</li>
<p class="indent90">Students&#8217; evaluation: 4.95 out of 5</p>
<li>PHIL 203, &#8220;Modern and Contemporary Philosophy&#8221; . . . . . . . . . . . . . Fall 2013</li>
<p class="indent90">Students&#8217; evaluation: 4.87 out of 5</p>
</ul>
<li>The Catholic University of America: Graduate Teaching Fellow and Adjunct Lecturer
<ul>
<li>Classical: PHIL 201, &#8220;The Classical Mind&#8221; . . . . . . . . . . . . . . . . . Falls 2006–2011, Summers 2008, 2011</li>
<p class="indent90">Students&#8217; evaluation: 9.5 out of 10</p>
<li>Logic: PHIL 351, &#8220;Introduction to Symbolic Logic&#8221; . . . . . . . . . . . . . . . . . . Spring 2012</li>
<p class="indent90">Students&#8217; evaluation: 8.4 out of 10</p>
<li>Modern: PHIL 202, &#8220;The Modern Mind&#8221; . . . . . . . . . . . . . . . . Springs 2007–2012, Fall 2007, Summer 2012</li>
<p class="indent90">Students&#8217; evaluation: 9.3 out of 10</p>
<li>Philosophy of Religion: PHIL 308, &#8220;Philosophy of God&#8221; . . . . . . . . . . . . . Summer 2011</li>
<p class="indent90">Students&#8217; evaluation: 8.9 out of 10</p>
<li>20<sup>th</sup> Century Philosophy: PHIL 454, &#8220;Contemporary Philosophy&#8221; . . . . . . . Spring 2011</li>
</ul>
</li>
</ul>
<p><strong>Theses Directed</strong></p>
<ul>
<li>McDaniel College
  <ul>
<li>Student: Patrick Keefe . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . Spring 2013</li>
<p class="indent90">Senior thesis on Zeno&#8217;s paradoxes and their solution via the infinitesimal calculus.</p>
</ul>
</li>
</ul>
<hr class="cv"/>
<h2 class="cv">Outreach, Service, and Invited Presentations</h2>
<p><strong>For Schools</strong></p>
<ul>
<li>Commencement address at Mount Sophia Academy&#8217;s High School Graduation Ceremonies</li>
<p class="indent60">Newark, DE: June 7<sup>th</sup>, 2014</p>
<li>Semi-weekly Skype sessions with the &#8220;Intro to Philosophy&#8221; class at Mount Sophia Academy</li>
<p class="indent60">Online: Spring Semester, 2014</p>
<li>Faculty advisor for Mount St. Mary&#8217;s University&#8217;s Filmmakers&#8217; Club</li>
<p class="indent60">Emmitsburg, MD: Academic Year 2012–2013.</p>
<li>Guest Lecture: &#8220;Aristotle and the Hellenistic Philosophers,&#8221; &#8220;World History and Philosophy&#8221; class at Mount Sophia Academy</li>
<p class="indent60">Newark, DE: October 15<sup>th</sup>, 2012</p>
</ul>
<p><strong>For Churches</strong></p>
<ul>
<li>Guest Sermon: &#8220;Is HMC a Living Contradiction?,&#8221; Hyattsville Mennonite Church</li>
<p class="indent60">Hyattsville, MD: July 7<sup>th</sup>, 2014</p>
<li>Member, Adult Education Committee, Hyattsville Mennonite Church and University Park Church of the Brethren</li>
<p class="indent60">Hyattsville, MD/University Park, MD: 2011–2014</p>
<li>Leader, Adult Education Bible Study Series, Hyattsville Mennonite Church and University Park Church of the Brethren</li>
<p class="indent60">University Park, MD: Springs 2010–2014</p>
<li>Chair, Adult Education Committee, Hyattsville Mennonite Church and University Park Church of the Brethren</li>
<p class="indent60">Hyattsville, MD/University Park, MD: 2012–2013 (fiscal year)</p>
<li>Leader, High School Sunday Bible Study Series, Hyattsville Mennonite Church and University Park Church of the Brethren</li>
<p class="indent60">University Park, MD: April 2013</p>
<li>Guest Sermon: &#8220;A Social Ontology of Vengeance,&#8221; Hyattsville Mennonite Church (meeting in the facilities of University Park Church of the Brethren)</li>
<p class="indent60">University Park, MD: September 23<sup>rd</sup>, 2012</p>
<li>Guest Sermon: &#8220;Family Stories,&#8221; Hyattsville Mennonite Church</li>
<p class="indent60">Hyattsville, MD: July 3<sup>rd</sup>, 2011</p>
</ul>
<hr class="cv"/>
<p class="aligncenter"><strong>Contact</strong></p>
<p class="aligncenter">micahtillman@<span class="displaynone">[removethis]</span>gmail.com</p>
<p class="alignright">[<a href="http://micahtillman.com/wp-content/uploads/2014/11/Tillman-CV-Web.pdf">Click here for PDF format</a>]</p>
</div><!--end content area-->
