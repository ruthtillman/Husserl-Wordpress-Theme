<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://micahtillman.com">Home</a>
      <a class="navbar-brand" href="http://micahtillman.com/blog">Blog</a>
      <a class="navbar-brand" href="http://micahtillman.com/cv">CV</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">About <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="http://micahtillman.com/about">About</a></li>
            <li><a href="http://micahtillman.com/writings">Writings</a></li>
            <li><a href="http://micahtillman.com/contact">Contact</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Links <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="http://twitter.com/ranilillanjum/philosophers-on-twitter">Philosophers on Twitter</a></li>
            <li><a href="http://www.historyofphilosophy.net/">The History of Philosophy Podcast</a></li>
            <li><a href="http://philosophybites.com/">Philosophy Bites Podcast</a></li>
            <li><a href="http://newbooksinphilosophy.com/">New Books in Philosophy Podcast</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Categories <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php wp_list_categories('title_li='); ?>
          </ul>
        </li>
        <li><a href="http://twitter.com/micahtillman"><img src="<?php bloginfo('template_url'); ?>/images/twitter.png" title="Follow me on Twitter" alt="Twitter"/></a></li>
        <li><a href="http://feeds.feedburner.com/micahtillman"><img src="<?php bloginfo('template_url'); ?>/images/rss.png" title="Subscribe to the blog" alt="Subscribe"/></a></li>
      </ul>
      <form class="navbar-form" method="get" action="<?php bloginfo('url'); ?>/" role="search">
        <div class="form-group">
          <input type="text" class="form-control" value="<?php the_search_query(); ?>" placeholder="Search" name="s" id="s">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
