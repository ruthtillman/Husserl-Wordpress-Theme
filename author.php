<?php get_header(); ?>
<?php get_template_part( 'navigation' ); ?>
<div class="container">
<?php if (have_posts()) : ?>

<?php $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); ?>

<div id="contentarea">
	<div class="thepostauthor">
	<?php echo get_avatar($curauth->user_email,80); ?>
		<div class="author_bio_page_meta">
		<h4>Posts by <?php echo $curauth->display_name ?></h4>
		<p><?php echo $curauth->description ?></p>
		</div>
	</div>

	<?php while (have_posts()) : the_post(); ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h3 class="headline"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
	<div class="entry">
		<div class="entry">
		<?php the_excerpt(); ?>
		<p>[<a href="<?php the_permalink() ?>">continue reading...</a>]</p>
	</div><!--end entry-->
	<p class="postmeta"><?php the_time('F j, Y'); ?></p>

	</div><!--end post-->
	<hr />
<?php endwhile; ?>

<?php else : ?>
	<div class="post">
	<div class="entry">
<p>Sorry, we couldn't find what you were looking for. You can try searching: <?php get_search_form(); ?></p>
<p>You can also visit the <a href="<?php echo home_url(); ?>">site's main page</a>.</p>	
	</div>
	</div>


<?php endif; ?>	

<div class="postnav">
	<span class="older"><?php next_posts_link('&larr; Older Entries', 0); ?></span>
	<span class="newer"><?php previous_posts_link('Newer Entries &rarr;', 0); ?></span>
</div>

</div><!--end content area-->
<?php get_footer(); ?>