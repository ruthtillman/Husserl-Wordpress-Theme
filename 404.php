<?php get_header(); ?>
<?php get_template_part( 'navigation' ); ?>
<div class="container">
	<div id="contentarea">

		<div class="post 404">
			<h1 class="headline">404 Error &ndash; Page Not Found</h1>
		<div class="entry">
			<p>Sorry, the link was broken or the page you tried to access was missing. To search for it, use the form below:</p>

				<?php get_search_form(); ?>

				<p>You can also visit the <a href="<?php echo home_url(); ?>">site's main page</a>.</p>
		</div><!--end entry-->
		</div><!--end post-->
	</div><!--end content area-->
<?php get_footer(); ?>