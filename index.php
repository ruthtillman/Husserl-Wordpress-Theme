<?php get_header(); ?>
<?php get_template_part( 'navigation' ); ?>
<div class="container .col-sm-12">
	<div id="contentarea">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h3 class="headline"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
	<div class="entry">
		<?php the_content('[click to continue...]'); ?>
		<?php wp_link_pages(); ?>
	</div><!--end entry-->
	<p class="postmeta"><?php the_time('F j, Y'); ?> &bull; <?php if (comments_open()) { comments_popup_link('Leave a Comment', '1 comment', '% comments', 'comments-link', ''); } else { ?>Comments are closed<?php } ?> &bull; <?php the_category(' &bull; '); ?> <?php edit_post_link('edit', ' &bull; [', ']'); ?></p>

	</div><!--end post-->

<?php endwhile; ?>

<?php else : ?>
	<div class="post">
	<div class="entry">
<p>Sorry, we couldn't find what you were looking for. You can try searching: <?php get_search_form(); ?></p>
<p>You can also visit the <a href="<?php echo home_url(); ?>">site's main page</a>.</p>	
	</div>
	</div>

<?php endif; ?>	

<div class="postnav">
	<span class="older"><?php next_posts_link('&larr; Older Entries', 0); ?></span>
	<span class="newer"><?php previous_posts_link('Newer Entries &rarr;', 0); ?></span>
</div>

</div><!--end content area-->
<?php get_footer(); ?>