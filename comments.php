<?php
 
// Do not delete these lines
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
die ('Please do not load this page directly. Thanks!');
 
if ( post_password_required() ) { ?>
<p class="nocomments">This post is password protected. Enter the password to view comments.</p>
<?php
return;
}
?>
 
<!-- You can start editing here. -->
 
<?php if ( have_comments() ) : ?> 
<div class="navigation">
<span class="older"><?php previous_comments_link() ?></span>
<span class="newer"><?php next_comments_link() ?></span>
</div>
 
<p id="comments"><?php comments_number('Leave a Comment', '1 Comment', '% Comments' );?></p>

<ol class="commentlist">
<?php wp_list_comments('avatar_size=60'); ?>
</ol>
 
<div class="navigation">
<div class="older"><?php previous_comments_link() ?></div>
<div class="newer"><?php next_comments_link() ?></div>
</div>
<?php else : // this is displayed if there are no comments so far ?>
 
<?php if ('open' == $post->comment_status) : ?>
<!-- If comments are open, but there are no comments. -->
 
<?php else : // comments are closed ?>
<!-- If comments are closed. -->
<?php if(!is_page()) { ?><p class="nocomments">Comments are closed.</p><?php } ?>
 
<?php endif; ?>
<?php endif; ?>
 
<?php if ('open' == $post->comment_status) : ?>
 

<?php comment_form('comment_notes_after='); ?> 
 
<?php endif; // if you delete this the sky will fall on your head ?>