<?php get_header(); ?>
<?php get_template_part( 'navigation' ); ?>
<div class="container">

<div id="contentarea">

<?php if (have_posts()) : ?>

<div class="archiveheadline">

<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

<?php /* If this is a category archive */ if (is_category()) { ?>

<h2 class="archivetitle">Archive of Posts in "<?php single_cat_title(); ?>"</h2>

<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>

<h2 class="archivetitle">Posts Tagged "<?php single_tag_title(); ?>"</h2>

<?php /* If this is a daily archive */ } elseif (is_day()) { ?>

<h2 class="archivetitle">Archive for <?php the_time('F jS, Y'); ?></h2>

<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>

<h2 class="archivetitle">Archive for <?php the_time('F, Y'); ?></h2>

<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>

<h2 class="archivetitle">Archive for <?php the_time('Y'); ?></h2>

<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>

<h2 class="archivetitle">Blog Archives</h2>

<?php } ?>

</div>

<?php while ( have_posts() ) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h3 class="headline"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
	<div class="entry">
		<div class="entry">
		<?php the_excerpt(); ?>
		<p>[<a href="<?php the_permalink() ?>">continue reading...</a>]</p>
	</div><!--end entry-->
	<p class="postmeta"><?php the_time('F j, Y'); ?></p>

	</div><!--end post-->
	<hr />
<?php endwhile; ?>

<?php else : ?>
	<div class="post">
	<div class="entry">
<p>Sorry, we couldn't find what you were looking for. You can try searching: <?php get_search_form(); ?></p>
<p>You can also visit the <a href="<?php echo home_url(); ?>">site's main page</a>.</p>	
	</div>
	</div>


<?php endif; ?>	

<div class="postnav">
	<span class="older"><?php next_posts_link('&larr; Older Entries', 0); ?></span>
	<span class="newer"><?php previous_posts_link('Newer Entries &rarr;', 0); ?></span>
</div>

</div><!--end content area-->
<?php get_footer(); ?>