<?php get_header(); ?>
<?php get_template_part( 'navigation' ); ?>
<div class="container">
	<div id="contentarea">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h1 class="headline"><?php the_title(); ?></h1>
	<div class="entry">
		<?php the_content(); ?>
		<?php wp_link_pages(); ?>
	</div><!--end entry-->
	</div><!--end post-->

<?php endwhile; ?>

<?php else : ?>
<p>Sorry, we couldn't find what you were looking for. You can try searching: <?php get_search_form(); ?></p>
<p>You can also visit the <a href="<?php echo home_url(); ?>">site's main page</a>.</p>

<?php endif; ?>	


</div><!--end content area-->
<?php get_footer(); ?>