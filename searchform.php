<form method="get" class="inlinesearch navbar-form" method="get" action="<?php bloginfo('url'); ?>/" role="search">
    <div class="form-group searchform">
      <input type="text" class="form-control" value="<?php the_search_query(); ?>" placeholder="Search" name="s" id="s">
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
</form>