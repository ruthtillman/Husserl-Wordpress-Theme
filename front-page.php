<?php get_header(); ?>
<?php get_template_part( 'navigation' ); ?>
<div class="container">
	<div id="contentarea">
	<div class="col-md-8 post entry">
<h2>Publications</h2>
<p>&bull; &#8220;Dallas Willard&#8217;s Husserlian Phenomenology,&#8221; in a text on the reception of Edmund Husserl&#8217;s thought in North America, ed. Carlo Ierna and Michaela Ferreri, in Contributions to Phenomenology (Dordrecht: Springer, forthcoming).</p>
<p>&bull; &#8220;Acts as Changes: A &#8216;Metabolic&#8217; Approach to the Philosophy of Action,&#8221; in Michael Sigrist and Roman Altshuler, eds., <em>Time and the Philosophy of Action</em> (New York: Routledge, forthcoming).</p>
<p>&bull; &#8220;Genesis 1&#8217;s Solution to the Euthyphro Problem,&#8221; <em>Philosophy &amp; Theology</em> 26, no. 1 (2014): 207–19.
<p>&bull; &#8220;Husserl&#8217;s Mereological Semiotics: Indication, Expression, Surrogation,&#8221; <em>The New Yearbook for Phenomenology and Phenomenological Philosophy</em> 12 (2012): 69–108.</p>
<p>&bull; &#8220;<a href="http://www.americandialectic.org/volume-ii-2012/no-2-may/husserls-genetic-philosophy-of-arithmetic/">Husserl&#8217;s Genetic Philosophy of Arithmetic</a>,&#8221; <em>American Dialectic</em> 2, no. 2 (May 2012): 141–90.</p>
<h2>Recent Courses</h2>
<p><em>McDaniel College</em>
<br/>&bull; &#8220;Minds and Language&#8221; Fall 2014
<br/>&bull; &#8220;Philosophy of Music&#8221; Spring 2014
<br/>&bull; &#8220;Elementary Logic&#8221; Spring 2014</p>
<p><em>University of Maryland, College Park</em>
<br/>&bull; &#8220;Contemporary Moral Issues&#8221; Fall 2014
<p><strong>[<a href="http://micahtillman.com/cv/">See more on my CV...</a>]</strong></p>
<h2>Recent Presentations</h2>
<p>&#8220;The Embarrassment of Punching Puppets: An Argument from Conversation for Freedom&#8221;</p>
<p style="padding-left: 20px;">Conference: Free Will<br />
Flint, MI: <em>Center for Cognition and Neuroethics</em>, October 10th-11th, 2014</p>
<p>&#8220;Acts as Changes: A &#8216;Metabolic&#8217; Approach to the Philosophy of Action&#8221;</p>
<p style="padding-left: 20px;">Conference: &#8220;Time and Agency&#8221;<br />
Washington, DC: <em>George Washington University</em>, November 18th, 2011</p>
<p>&#8220;Do Numerals Mean Anything?: Husserl&#8217;s Theory of Signs in &#8216;Mechanical&#8217; Calculation&#8221;</p>
<p style="padding-left: 20px;">Conference: &#8220;The Early Phenomenology of Munich and G&ouml;ttingen&#8221;<br />
Steubenville, OH: <em>Franciscan University of Steubenville</em>, April 30th, 2011</p>
<p><strong>[<a href="http://micahtillman.com/cv/">See more on my CV...</a>]</strong></p>
	</div><!--end column 1-->
	<div class="col-md-4 post entry">
<p><img src="<?php bloginfo('template_url'); ?>/images/micah.jpg" alt="" title="Micah Tillman" class="alignleft" /></p>
<h2>About Micah</h2>
<p>I love teaching philosophy. I believe that philosophy&#8217;s job is to help us think clearly about our lives, so my teaching emphasizes the relevance of philosophical texts and concepts to the world and culture in which my students live.</p>

<p>I specialize in phenomenology, but enjoy teaching and researching contemporary Analytic and Continental philosophy, as well as the history of philosophy. [<a href="http://micahtillman.com/about/">More about Micah...</a>]</p>
	</div><!--end column 2-->

	</div><!--end content area-->
<?php get_footer(); ?>